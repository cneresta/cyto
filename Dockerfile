#FROM rocker/tidyverse:3.5.2
FROM openanalytics/r-base
RUN apt-get -y update
RUN apt-get -y install libcurl4-openssl-dev
RUN apt-get -y install libssl-dev 
RUN apt-get -y install libxml2-dev
RUN apt-get -y install libfontconfig1-dev
RUN apt-get -y install libcairo2-dev
RUN apt-get -y install libharfbuzz-dev
RUN apt-get -y install libfribidi-dev
RUN apt-get -y install libgit2-dev
RUN apt-get -y install libfreetype6-dev
RUN apt-get -y install libpng-dev
RUN apt-get -y install libtiff5-dev
RUN apt-get -y install libjpeg-dev
RUN apt-get -y install libharfbuzz-dev
RUN apt-get -y install libfribidi-dev 
RUN apt-get -y install libfreetype6-dev libpng-dev libtiff5-dev libjpeg-dev

RUN R -e 'install.packages("remotes", dependencies=TRUE, repos="http://cran.rstudio.com/")'
RUN R -e 'remotes::install_github("r-lib/remotes", ref = "97bbf81")'
RUN R -e 'install.packages(c("gdtools","shiny","golem","attempt","DT","glue","htmltools","processx","thinkr","shinydashboard","dplyr","tidyverse","beeswarm","plotly","rlist","waiter","Rlabkey"), repos="http://cran.rstudio.com/")'

#COPY Rprofile.site /usr/lib/R/etc/
COPY VASICyto_*.tar.gz /app.tar.gz
RUN R -e 'remotes::install_local("/app.tar.gz")'

COPY Rprofile.site /usr/lib/R/etc/
EXPOSE 8080
#CMD  ["R", "-e options('shiny.port'=3838,shiny.host='0.0.0.0'); VASICyto::run_app()"]
CMD  ["R", "-e", "VASICyto::run_app(prod = TRUE)"]
