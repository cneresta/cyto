
<!-- README.md is generated from README.Rmd. Please edit that file -->

# VASICyto

<!-- badges: start -->

[![Lifecycle:
experimental](https://img.shields.io/badge/lifecycle-experimental-orange.svg)](https://www.tidyverse.org/lifecycle/#experimental)
[![Travis build
status](https://travis-ci.org/SalebHet/VASICyto.svg?branch=master)](https://travis-ci.org/SalebHet/VASICyto)
[![AppVeyor build
status](https://ci.appveyor.com/api/projects/status/github/SalebHet/VASICyto?branch=master&svg=true)](https://ci.appveyor.com/project/SalebHet/VASICyto)
<!-- badges: end -->

VASICyto is a WebApp made to visualize cytometric data based on the Work
of SISTM Team.

## Installation

You can install the released version of VASICyto from
[CRAN](https://CRAN.R-project.org) with:

``` r
install.packages("VASICyto")
```

Source can be found here: <https://gitlab.inria.fr/cneresta/VASICyto>

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(VASICyto)
## basic example code
```

What is special about using `README.Rmd` instead of just `README.md`?
You can include R chunks like so:

You’ll still need to render `README.Rmd` regularly, to keep `README.md`
up-to-date.

You can also embed plots, for example:

In that case, don’t forget to commit and push the resulting figure
files, so they display on GitHub!
