#' @import shiny
#' @import shinydashboard

library(shinydashboard)
app_ui <- function() {
  cat(str(app_prod()))
  # Leave this function for adding external resources
  golem_add_external_resources()
  #browser()
  shinydashboard::dashboardPage(
    # List the first level UI elements here 
     shinydashboard::dashboardHeader(title = "Flow Dashboard"),
     
     shinydashboard::dashboardSidebar(
       div(
        fileInput("fileInput", "fileInput", multiple = FALSE, accept = NULL, width = NULL),
                                                                         
        radioButtons("fileType", "Select File Type",
                    choices = list("xlsx"="xlsx","txt"="txt","csv"="csv"), selected = "xlsx"),
                                                                         
                                                                         
        selectInput("StimulationColumn", "Select Antigen Stimulation Column", choices = c()),
        selectInput("TreatmentColumn", "Select Treatment arm Column", choices = c()),
        selectInput("TimePoint", "Select TimePoint Column", choices=c()),
        selectInput("patient","Select Participant ID Column",choices = c()),
        selectInput("marginal","Select Marginal Cytokine Expresseion",choices = c(),multiple = TRUE),
        selectInput("boolean","Select Boolean Cytokine Expresseion",choices = c(),multiple = TRUE)
       )

       ),
     shinydashboard::dashboardBody(
       
       tabsetPanel(
         tabPanel("DataSet", dataSetUI()),
         tabPanel("Marginal",totalBoxUI()),
         tabPanel("Boolean",multiBoxUI()),
         tabPanel("Polyfunctionality",polyfuncUI())
       )
       
      )
    )
}

#' @import shiny
golem_add_external_resources <- function(){
  
  addResourcePath(
    'www', system.file('app/www', package = 'VASICyto')
  )
 
  # tags$head(
  #   golem::activate_js(),
  #   golem::favicon()
  #   # Add here all the external resources
  #   # If you have a custom.css in the inst/app/www
  #   # Or for example, you can add shinyalert::useShinyalert() here
  #   #tags$link(rel="stylesheet", type="text/css", href="www/custom.css")
  # )
}
