context("Selenium Test")

library(RSelenium)
library(testthat)
library(processx)


waitFor <- function(how,id){
  #webElem <- NULL
  
  i <- 0
  while(i<=1000){
    webElem <<- tryCatch({remDr$findElement(using = how, value = id)},
                         error = function(e){NULL})
    #loop until element with name <value> is found in <webpage url>
    
    i <- i+1
    if(!is.null(webElem)){
      break()
    }
  }
  if(!is.null(webElem)){
      return(webElem)
    
  }
  else{
    stop(paste0(id," not found \n"))
    #cat(paste0(id," not found \n"))
  }
}


wd <- getwd()

#Uncomment this for local test


remDr <- RSelenium::remoteDriver(remoteServerAddr = "localhost",
                                 port = 4445L,
                                 browserName = "firefox")
remDr$open()
appURL <- "http://shiny:3838"

test_that("can connect to app", {
  remDr$navigate("http://shiny:3838")
  webElem <- waitFor("xpath","/html/body/div/header/span")#remDr$findElement(using = "xpath", value = "/html/body/div[2]/h2")
  textWebElem <- webElem$getElementText()
  expect_equal(as.character(textWebElem), "Flow Dashboard")

})


test_that("Total Cytokine",{
  remDr$navigate(appURL)
  onglet <- waitFor("xpath","/html/body/div/div/section/div/ul/li[2]/a")
  onglet$clickElement()
  testBtn <- waitFor("id","test1")
  testBtn$clickElement()
  computeBtn <- waitFor("id","compute1")
  computeBtn$clickElement()
  # ongletResult <- waitFor("xpath","/html/body/div/div/section/div/div/div[2]/div/ul/li[2]/a")
  # 
  # while(! ongletResult$isElementDisplayed()[[1]]){
  #   ongletResult <- waitFor("xpath","/html/body/div/div/section/div/div/div[2]/div/ul/li[2]/a")
  # }
  # ongletResult$clickElement()
  participant <- waitFor("xpath","/html/body/div/div/section/div/div/div[2]/div/div/div[2]/div/div/div[1]/div/div/div[4]/div[2]/table/tbody/tr[1]/td[2]")
  expect_equal(as.character(participant$getElementText()),"A1")
  
  chartBtn <- waitFor("xpath","/html/body/div/div/section/div/div/div[2]/div/div/div[2]/div/ul/li[2]/a")
  while(! chartBtn$isElementDisplayed()[[1]]){
    chartBtn <- waitFor("xpath","/html/body/div/div/section/div/div/div[2]/div/div/div[2]/div/ul/li[2]/a")
  }
  
  chartBtn$clickElement()
  img <- waitFor("xpath","/html/body/div/div/section/div/div/div[2]/div/div/div[2]/div/div/div[2]/div/img")
})



test_that("Individual Cytokine",{
  remDr$navigate(appURL)
  IndivCyto <- waitFor("xpath","/html/body/div/div/section/div/ul/li[3]/a")
  IndivCyto$clickElement()
  testBtn <- waitFor("id","test2")
  testBtn$clickElement()
  computeBtn <- waitFor("id","compute2")
  computeBtn$clickElement()
  participant <- waitFor("xpath","/html/body/div/div/section/div/div/div[3]/div/div/div[2]/div/div/div[1]/div/div/div[4]/div[2]/table/tbody/tr[1]/td[5]")
  expect_equal(as.character(participant$getElementText()),"A1")
  chartBtn <- waitFor("xpath","/html/body/div/div/section/div/div/div[3]/div/div/div[2]/div/ul/li[2]/a")
  while(! chartBtn$isElementDisplayed()[[1]]){
    chartBtn <- waitFor("xpath","/html/body/div/div/section/div/div/div[3]/div/div/div[2]/div/ul/li[2]/a")
  }
  chartBtn$clickElement()
  img <- waitFor("xpath","/html/body/div/div/section/div/div/div[3]/div/div/div[2]/div/div/div[2]/div/img")
})

test_that("polyFunc",{
  remDr$navigate(appURL)
  pfuncTab <- waitFor("xpath","/html/body/div/div/section/div/ul/li[4]/a")
  pfuncTab$clickElement()
  testBtn <- waitFor("id","test3")
  testBtn$clickElement()
  cptBtn <- waitFor("id","computeBtn")
  cptBtn$clickElement()
  val <- waitFor("xpath","/html/body/div/div/section/div/div/div[4]/div/div/div[2]/div/div/div[1]/div/div/div[4]/div[2]/table/tbody/tr[1]/td[3]")
  expect_equal(as.character(val$getElementText()),"0.00995732")
  chartTab <- waitFor("xpath","/html/body/div/div/section/div/div/div[4]/div/div/div[2]/div/ul/li[2]/a")
  chartTab$clickElement()
  histo <- waitFor("xpath","/html/body/div/div/section/div/div/div[4]/div/div/div[2]/div/div/div[2]/div/div[1]/div/div/div[2]/div/img")
  
})